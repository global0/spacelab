This is a play area for experimenting git commands and using git as source code management.

git status
git add <filepath>
git commit -m <message>

git help log

git add .
git diff --cached

git branch
git branch <newBranchName> [oldBranchName]
git checkout <branchName>
git branch -m <newBranch/name> [oldBranchName]

git branch -d <oldBranchName>

git push
git pull
git pull --rebase
